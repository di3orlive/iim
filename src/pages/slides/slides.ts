import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the SlidesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-slides',
    templateUrl: 'slides.html',
})
export class SlidesPage {
    slides = [
        {
            title: "Доброго дня",
            image: "assets/services/масаж_в_4_руки.jpg",
        },
        {
            title: "Що таке масаж? Масаж це ми.",
            image: "assets/services/стоун.jpg",
        },
        {
            title: "Хто це ми? Професіонали своєї справи.",
            image: "assets/services/масаж_в_4_руки_2.jpg",
        }
    ];
    
    
    constructor(public nav: NavController, public navParams: NavParams) {
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad SlidesPage');
    }
    
    
    
    
    goToHome(){
        this.nav.setRoot(TabsPage);
    }
}
