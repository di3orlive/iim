import {Component} from '@angular/core';
import {NavController} from "ionic-angular";
import {SafeSubscribe} from "../../app/helpers/safe-subscripe/safe-subscripe";
import {ContactsArticlePage} from "../contacts-article/contacts-article";
import {CommonService} from "../../app/services/common";

@Component({
    selector: 'page-contacts',
    templateUrl: 'contacts.html'
})
export class ContactsPage extends SafeSubscribe {
    contacts: any;


    constructor(
        private navCtrl: NavController,
        private commonService: CommonService
    ) {
        super();
        this.commonService.contactsAsync.safeSubscribe(this, (value) => {
            this.contacts = value;
        });
    }
    
    goToOtherPage(item) {
        this.navCtrl.push(ContactsArticlePage, {params: item});
    }
}
