import {Component} from '@angular/core';
import {NativePageTransitions, NativeTransitionOptions} from '@ionic-native/native-page-transitions';
import {ServicesPage} from '../services/services';
import {ContactsPage} from '../contacts/contacts';
import {CalendarPage} from '../calendar/calendar';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    loaded = false;
    tabIndex = 0;
    
    calendar = CalendarPage;
    services = ServicesPage;
    contacts = ContactsPage;
    
    constructor(private nativePageTransitions: NativePageTransitions) {
    }
  
    
    private getAnimationDirection(index):string {
        let currentIndex = this.tabIndex;
        
        this.tabIndex = index;
        
        switch (true){
            case (currentIndex < index):
                return('left');
            case (currentIndex > index):
                return ('right');
        }
    }
    
    public transition(e):void {
        let options: NativeTransitionOptions = {
            direction: this.getAnimationDirection(e.index),
            duration: 250,
            slowdownfactor: -1,
            slidePixels: 0,
            iosdelay: 20,
            androiddelay: 0,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 48
        };
        
        if (!this.loaded) {
            this.loaded = true;
            return;
        }
    
        console.log(e);
    
        this.nativePageTransitions.slide(options);
    }
}
