import {Component, OnInit} from '@angular/core';
import {ToastController, ViewController} from "ionic-angular";
import {Calendar} from "@ionic-native/calendar";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";
import {ServicesService} from "../../services/services.service";
import {CommonService} from "../../services/common";

@Component({
    selector: 'app-event-card',
    templateUrl: './add-event.pop.html'
})
export class AddEventPop extends SafeSubscribe implements OnInit {
    add = {
        name: '',
        phone: '',
        email: '',
        additional: ''
    };
    dateStart: Date;
    dateEnd: Date;
    selectedService = {
        summary: '',
        img: '',
        time: 0,
    };
    services: any;
    mask = ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    contacts = [];
    masseur: any;
    
    
    constructor(
        public viewCtrl: ViewController,
                private toastCtrl: ToastController,
                private calendar: Calendar,
                private servicesService: ServicesService,
                private commonService: CommonService
    ) {
        super();
        this.servicesService.servicesAsync.safeSubscribe(this, (value) => {
            this.services = value;
        });
        this.commonService.contactsAsync.safeSubscribe(this, (value) => {
            this.contacts = value;
        });
        
        
        this.dateStart = this.viewCtrl.data.data;
        this.masseur = this.viewCtrl.data.masseur;
        console.log(this.masseur);
    }
    
    
    ngOnInit() {
    }
    
    
    save(f) {
        if (f.invalid) {
            return;
        }
        
        
        // colorId = https://eduardopereira.pt/wp-content/uploads/2012/06/google_calendar_api_event_color_chart.png
        let body = {
            location: null,
            // status: 'confirmed',
            visibility: 'private',
            colorId: 11,
            summary: this.selectedService.summary,
            description: `name: ${this.add.name}
${this.add.phone ? 'phone: ' + this.add.phone : ''}
${this.add.email ? 'email: ' + this.add.email : ''}
${this.add.additional ? 'additional: ' + this.add.additional : ''}`,
            start: {dateTime: new Date(this.dateStart).toISOString()},
            end: {dateTime: new Date(this.dateEnd).toISOString()}
        };
        

    
        this.contacts.forEach((item) => {
            if (item.id === this.masseur) {
                body.location = item.address;
            }
        });
        
        
        
        console.log(body);
    
    
    
        this.calendar.createEvent(this.selectedService.summary, body.location, null, new Date(this.dateStart), new Date(this.dateEnd)).then((res) => {
            console.log(res);


            this.calendar.hasWritePermission().then((res) => {
                if (res) {
                    let toast = this.toastCtrl.create({
                        message: 'Масаж додано також в ваш календар',
                        duration: 2000,
                        showCloseButton: true,
                        closeButtonText: 'Ok'
                    });
                    toast.present();
                }
            });
        }, (err) => {
            console.log(err);


            this.calendar.hasWritePermission().then((res) => {
                if (res) {
                    let toast = this.toastCtrl.create({
                        message: 'Масаж додано також в ваш календар',
                        duration: 2000,
                        showCloseButton: true,
                        closeButtonText: 'Ok'
                    });
                    toast.present();
                }
            });
        });
        
        this.viewCtrl.dismiss(body);
    }
    
    close() {
        this.viewCtrl.dismiss();
    }
    
    
    onSelectService() {
        this.dateEnd = new Date(new Date(this.dateStart).getTime() + (this.selectedService.time * 60000));
        console.log(this.dateEnd);
    }
}
