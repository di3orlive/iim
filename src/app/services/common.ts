import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class CommonService {
    mapOptions = new BehaviorSubject<any>(this.getDefaultMapOptions());
    isOnline = new BehaviorSubject<any>(true);
    contacts = new BehaviorSubject<any>(this.getContacts());
    
    constructor(
    ) {
        this.setOnline(navigator.onLine);
        setInterval(() => {
            this.setOnline(navigator.onLine);
        }, 5000);
    }
    
    
    
    
    get contactsAsync() {
        return this.contacts.asObservable().distinctUntilChanged();
    }
    
    setContacts(a) {
        this.contacts.next(a);
    }
    
    getContacts() {
        return [
            {
                id: 'inna',
                name: 'Інна',
                img: './assets/massagists/inna.jpg',
                address: 'вулиця Сумгаїтська, 69',
                address_position: {lat: 49.429899, lng: 32.010720},
                about: 'ЧНУ ім.Б. Хмельницького ННІ фізичної культури , спорту і здоров\'я. Масажист-реабілітолог',
                phone: '(096) 201-57-38'
            },
            {
                id: 'ira',
                name: 'Іра',
                img: './assets/massagists/ira.jpg',
                address: 'вулиця Героїв Дніпра, 23',
                address_position: {lat: 49.437111, lng: 32.091479},
                about: 'ЧНУ ім.Б. Хмельницького ННІ фізичної культури , спорту і здоров\'я. Масажист-реабілітолог',
                phone: '(068) 635-67-82',
                inst: 'iryna_kompaniits_',
            }
        ];
    }
    
    
    
    
    get mapOptionsAsync() {
        return this.mapOptions.asObservable().distinctUntilChanged();
    }
    
    setMapObj(a) {
        this.mapOptions.next(a);
    }
    
    getDefaultMapOptions() {
        return {
            center: {lat: 49.429899, lng: 32.010720},
            disableDefaultUI: true,
            zoom: 15,
            styles: [
                {
                    "featureType": "administrative",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 65
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": "50"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "30"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "40"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ffff00"
                        },
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -97
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -100
                        }
                    ]
                }
            ]
        }
    }
    
    
    
    
    get isOnlineAsync() {
        return this.isOnline.asObservable().distinctUntilChanged();
    }
    
    setOnline(a) {
        this.isOnline.next(a);
    }
}













