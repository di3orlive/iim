import {Component} from '@angular/core';
import {Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {Network} from "@ionic-native/network";
import {SplashScreen} from '@ionic-native/splash-screen';
import {TabsPage} from '../pages/tabs/tabs';
import {SlidesPage} from "../pages/slides/slides";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any;
    
    constructor(
        platform: Platform,
        statusBar: StatusBar,
        splashScreen: SplashScreen,
        network: Network
    ) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
    
    
            network.onDisconnect().subscribe(() => {
                console.log('network was disconnected :-(');
            });

            network.onConnect().subscribe(() => {
                console.log('network connected!');
            });
            
        
            this.rootPage = TabsPage;
            if (!localStorage.getItem('SlidesPage')) {
                localStorage.setItem('SlidesPage', JSON.stringify(true));
                this.rootPage = SlidesPage;
            }
            // this.rootPage = SlidesPage;
        });
    }
}
